<!DOCTYPE html>
<html lang="en">
<head>


<script type="text/javascript" async="" src="js/analytics.js"></script><script async="" src="other/js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-49509616-2');
</script>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <title>RVD Education</title>

    <!-- Bootstrap Css file -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    
    <!-- Animate Css file -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">

    <!-- Basic css File -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Jquery Js File -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Slick Css -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">

    <link rel="shortcut icon" type="image/png" href="Images/fav.jpg">
    <link href="css/pnotify.css" rel="stylesheet">
    <style type="text/css">

    .err { color:red;}
    p { font-size: 17px;}

    </style>

    
    
  
</head>

    <body id="home">

<!-- Navigation Start -->
<nav class="navbar navbar-default navbar-fixed-top" id="nav-main">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/#"><div class="logo"></div></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#home" class="page-scroll">Home</a></li>
                <li><a href="#about" class="page-scroll">About Us</a></li>
                <li><a href="#serv" class="page-scroll">Services</a></li>
                <li><a href="#proc" class="page-scroll">Process</a></li>
                
                <li><a href="#testi" class="page-scroll">Testimonials</a></li>
                <li><a href="#contact" class="page-scroll">Contact Us</a></li>
                <li><a href="career" class="page-scroll">Career</a></li>
            </ul>
        </div>
    </div>
</nav><!--Navigation End -->



<!-- Hero Start -->
<section>
    <div class="hero-main-container">
        <div class="hero-data animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="0">
            We are from RVD Education
        </div>
    </div>
</section>


<!-- Hero End -->


<!-- About Start -->
<section id="about">
    <div class="about-main-container section-container">
        <div class="container">
            <div class="about-inner">
                <div class="heading animated" data-animation="fadeInDown" data-animation-delay="0">
                    <span>
                        About Us
                    </span>
                </div>
               

                <h3 style="text-align:center;"> Our Strength </h3>

                <ul class="about-list">
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="200">
                        <div class="about-list-image">
                            <img src="images/about-1.png">
                        </div>
                        <div class="about-list-data">
                            <h4>Web &amp; Mobile Apps</h4>
                            <p>We love baking wonderful web and mobile apps. </p>
                        </div>
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="300">
                        <div class="about-list-image">
                            <img src="images/about-2.png">
                        </div>
                        <div class="about-list-data">
                            <h4>Transparency</h4>
                            <p>Believe what you see.We maintain code live so clients can track development anytime.</p>
                        </div>
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="400">
                        <div class="about-list-image">
                            <img src="images/about-3.png">
                        </div>
                        <div class="about-list-data">
                            <h4>Best Quality</h4>
                            <p>We understand things from user's point of view, so we always comes up with the best and easiest solution.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- About End -->

<!-- Services Start -->
<section id="serv">
    <div class="services-main-container section-container">
        <div class="container">
            <div class="services-inner">
                <div class="services-left">
                    <div class="heading animated" data-animation="fadeInDown" data-animation-delay="0">Services</div>
                    <div class="services-data animated" data-animation="fadeIn" data-animation-delay="200">
                        <h3>Tailored digital solutions and true service.</h3>
                        <p>What we do is simple. How we do it, is complex (but let us worry about that!). </p>
                        <p> We create great design, and implement innovative development on a stable CMS. </p>

                        <p> 
                            Our spectrum of services include top quality web designing and development, customized web applications, E-Commerce, Laravel Websites, Responsive web designs, SEO, Social Media marketing.We can also design your content by analyzing your business and inputs.</p>
                    </div>
                </div>
                <div class="services-right">
                    <ul class="services-list active">
                        <li>
                            <div class="services-list-inner">
                                <div class="service-list-data">
                                    <div class="service-image">
                                        <img src="images/serv-1.png">
                                    </div>
                                    <p>Web Design &amp;
                                        Web Development</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="services-list-inner">
                                <div class="service-list-data">
                                    <div class="service-image">
                                        <img src="images/serv-2.png">
                                    </div>
                                    <p>Android &amp; ios
                                        Development</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="services-list-inner">
                                <div class="service-list-data">
                                    <div class="service-image">
                                        <img src="images/serv-3.png">
                                    </div>
                                    <p>Graphic Design
                                        &amp; Company Branding</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="services-list-inner">
                                <div class="service-list-data">
                                    <div class="service-image">
                                        <img src="images/serv-4.png">
                                    </div>
                                    <p>SEO &amp;
                                        Digital Marketing</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services End  -->

<!-- Process Start -->
<section id="proc">
    <div class="process-main-container section-container">
        <div class="container">
            <div class="process-inner">
                <div class="heading animated" data-animation="fadeInDown" data-animation-delay="0">
                    Process
                </div>
                <div class="process-data animated" data-animation="fadeIn" data-animation-delay="0">
                    <h3>The smart and easy way to do the quality work.</h3>
                    <ul class="process-list" style="min-height: 48px;">
                        <li class="faded active">
                            <p>Together, we analyze your requirements and provide you a detailed work-flow for your project.</p>
                        </li>
                        <li class="">
                            <p>Once we get clear about the requirements, we start making your dream into reality. </p>
                        </li>
                        <li class="">
                            <p>One of the most important part in any project is client communication. We know that very well.</p>
                        </li>
                        <li class="">
                            <p>Review is a constant process. We always keep code live for our clients to track status.</p>
                        </li>
                        
                        <li class="">
                            <p>Once we get the acceptance on current module, we move ahead to next one after creating backup on bitbucket.</p>
                        </li>
                        <li class="">
                            <p>Upon finishing all modules, we handover your baby and wish a grand success for your venture.</p>
                        </li>
                    </ul>
                </div>
                <div class="process-control">
                    <div class="process-control-line">
                        <div class="process-control-line-active" style="width: 0px;"></div>
                    </div>
                    <ul class="process-control-list">
                        <li class="active animated" data-animation="fadeInLeft" data-animation-delay="0">
                            <div class="process-control-list-image">
                                <img src="images/pro-1.png">
                            </div>
                            <div class="process-control-list-data">
                                <div class="process-control-list-round">

                                </div>
                                <p>Planning</p>
                            </div>
                        </li>
                        <li class="animated" data-animation="fadeInLeft" data-animation-delay="150">
                            <div class="process-control-list-image">
                                <img src="images/pro-2.png">
                            </div>
                            <div class="process-control-list-data">
                                <div class="process-control-list-round">

                                </div>
                                <p>Development</p>
                            </div>
                        </li>
                        <li class="animated" data-animation="fadeInLeft" data-animation-delay="450">
                            <div class="process-control-list-image">
                                <img src="images/pro-4.png">
                            </div>
                            <div class="process-control-list-data">
                                <div class="process-control-list-round">

                                </div>
                                <p>QA</p>
                            </div>
                        </li>
                        <li class="animated" data-animation="fadeInLeft" data-animation-delay="300">
                            <div class="process-control-list-image">
                                <img src="images/pro-3.png">
                            </div>
                            <div class="process-control-list-data">
                                <div class="process-control-list-round">

                                </div>
                                <p>Review</p>
                            </div>
                        </li>
                        
                        <li class="animated" data-animation="fadeInLeft" data-animation-delay="600">
                            <div class="process-control-list-image">
                                <img src="images/pro-5.png">
                            </div>
                            <div class="process-control-list-data">
                                <div class="process-control-list-round">

                                </div>
                                <p>Acceptance</p>
                            </div>
                        </li>
                        <li class="animated" data-animation="fadeInLeft" data-animation-delay="750">
                            <div class="process-control-list-image">
                                <img src="images/pro-6.png">
                            </div>
                            <div class="process-control-list-data">
                                <div class="process-control-list-round">

                                </div>
                                <p>Going Live</p>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Process End -->

<!-- Development Start -->
<section>
    <div class="development-main-container section-container">
        <div class="container">
            <div class="development-inner">
                <div class="heading animated" data-animation="fadeInDown" data-animation-delay="0">
                    Our Development Stacks
                </div>
                <ul class="development-list">
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="0">
                        <img src="images/dev-1.png">
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="150">
                        <img src="images/dev-2.png">
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="300">
                        <img src="images/dev-3.png">
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="450">
                        <img src="images/dev-4.png">
                    </li>
                    
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="750">
                        <img src="images/dev-6.png">
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="900">
                        <img src="images/dev-7.png">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Development End -->

<!-- News Start -->


<!-- Testimonial Start -->
<section id="testi">
    <div class="testimonial-main-container section-container">
        <div class="container">
            <div class="testimonial-inner">
                <div class="heading animated" data-animation="fadeInDown" data-animation-delay="0">
                    Testimonials
                </div>
                <div class="testimonial-info">
                    Hear about us from others.
                </div>
                <div class="testimonial-slider-container">
                    <div class="testimonial-slider animated slick-initialized slick-slider" data-animation="fadeIn" data-animation-delay="0" role="toolbar">

                        <div aria-live="polite" class="slick-list draggable" style="padding: 0px;"><div class="slick-track" role="listbox" style="opacity: 1; width: 2880px; left: -720px;"><div class="testimonial-slider-one slick-slide slick-cloned" data-slick-index="-4" aria-hidden="true" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    We are building am portal for our company, these guys are very professional and know the job. Very happy with the output and over all communication. They don't just code, but give you suggestions, which is handy at times. Highly Recommefonded. 
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://travelplug.in" target="_blank" tabindex="-1"><img src="images/tp_logo.jpg"> </a>
                                    </div>
                                     <h5><a href="http://travelplug.in" target="_blank" tabindex="-1"> Travel Plug </a></h5>  

                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-cloned" data-slick-index="-3" aria-hidden="true" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    We were in a search of an agency who can manage our digital marketing as well, these guys improved our ranking in no time and we got many useful leads.
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://maxdomachinery.com/" target="_blank" tabindex="-1"> <img src="images/maxdo_logo.jpg"> </a>
                                    </div>
                                   <h5><a href="http://maxdomachinery.com/" target="_blank" tabindex="-1">  MaxDo Machinery, China </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    I wanted a fresh and interactive look for my new website. Anglara Provided me just that. Highly recommended.
                                </div>
                                <div class="testimonial-user">
                                     <div class="testimonial-img">
                                        <a href="http://massinternational.in/" target="_blank" tabindex="-1"> <img src="images/mass_logo.jpg"> </a>
                                    </div>
                                    <h5><a href="http://massinternational.in/" target="_blank" tabindex="-1"> Mass International </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-cloned slick-active" data-slick-index="-1" aria-hidden="false" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    

                                    Glad we could get the job done from a highly experience and creative team. Thanks Guys. 
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://yylpolishing.com/" target="_blank" tabindex="0"><img src="images/yyl.jpg"> </a>
                                    </div>
                                    <h5><a href="http://yylpolishing.com/" target="_blank" tabindex="0">YYL Polishing, China </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-current slick-active slick-center" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    We are building am portal for our company, these guys are very professional and know the job. Very happy with the output and over all communication. They don't just code, but give you suggestions, which is handy at times. Highly Recommefonded. 
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://travelplug.in" target="_blank" tabindex="0"><img src="images/tp_logo.jpg"> </a>
                                    </div>
                                     <h5><a href="http://travelplug.in" target="_blank" tabindex="0"> Travel Plug </a></h5>  

                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-active" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide01" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    We were in a search of an agency who can manage our digital marketing as well, these guys improved our ranking in no time and we got many useful leads.
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://maxdomachinery.com/" target="_blank" tabindex="0"> <img src="images/maxdo_logo.jpg"> </a>
                                    </div>
                                   <h5><a href="http://maxdomachinery.com/" target="_blank" tabindex="0">  MaxDo Machinery, China </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    I wanted a fresh and interactive look for my new website. Anglara Provided me just that. Highly recommended.
                                </div>
                                <div class="testimonial-user">
                                     <div class="testimonial-img">
                                        <a href="http://massinternational.in/" target="_blank" tabindex="-1"> <img src="images/mass_logo.jpg"> </a>
                                    </div>
                                    <h5><a href="http://massinternational.in/" target="_blank" tabindex="-1"> Mass International </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    

                                    Glad we could get the job done from a highly experience and creative team. Thanks Guys. 
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://yylpolishing.com/" target="_blank" tabindex="-1"><img src="images/yyl.jpg"> </a>
                                    </div>
                                    <h5><a href="http://yylpolishing.com/" target="_blank" tabindex="-1">YYL Polishing, China </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-cloned slick-center" data-slick-index="4" aria-hidden="true" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    We are building am portal for our company, these guys are very professional and know the job. Very happy with the output and over all communication. They don't just code, but give you suggestions, which is handy at times. Highly Recommefonded. 
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://travelplug.in" target="_blank" tabindex="-1"><img src="images/tp_logo.jpg"> </a>
                                    </div>
                                     <h5><a href="http://travelplug.in" target="_blank" tabindex="-1"> Travel Plug </a></h5>  

                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-cloned" data-slick-index="5" aria-hidden="true" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    We were in a search of an agency who can manage our digital marketing as well, these guys improved our ranking in no time and we got many useful leads.
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://maxdomachinery.com/" target="_blank" tabindex="-1"> <img src="images/maxdo_logo.jpg"> </a>
                                    </div>
                                   <h5><a href="http://maxdomachinery.com/" target="_blank" tabindex="-1">  MaxDo Machinery, China </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-cloned" data-slick-index="6" aria-hidden="true" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    I wanted a fresh and interactive look for my new website. Anglara Provided me just that. Highly recommended.
                                </div>
                                <div class="testimonial-user">
                                     <div class="testimonial-img">
                                        <a href="http://massinternational.in/" target="_blank" tabindex="-1"> <img src="images/mass_logo.jpg"> </a>
                                    </div>
                                    <h5><a href="http://massinternational.in/" target="_blank" tabindex="-1"> Mass International </a></h5>
                                </div>
                            </div>
                        </div><div class="testimonial-slider-one slick-slide slick-cloned" data-slick-index="7" aria-hidden="true" tabindex="-1" style="width: 240px;">
                            <div class="testimonial-slider-inner">
                                <div class="testimonial-data">
                                    

                                    Glad we could get the job done from a highly experience and creative team. Thanks Guys. 
                                </div>
                                <div class="testimonial-user">
                                    <div class="testimonial-img">
                                        <a href="http://yylpolishing.com/" target="_blank" tabindex="-1"><img src="images/yyl.jpg"> </a>
                                    </div>
                                    <h5><a href="http://yylpolishing.com/" target="_blank" tabindex="-1">YYL Polishing, China </a></h5>
                                </div>
                            </div>
                        </div></div></div>
                       
                        
                        
                        
                        
                    <ul class="slick-dots" style="display: flex;" role="tablist"><li class="slick-active" aria-hidden="false" role="presentation" aria-selected="true" aria-controls="navigation00" id="slick-slide00"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">1</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation01" id="slick-slide01"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">2</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation02" id="slick-slide02"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">3</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation03" id="slick-slide03"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">4</button></li></ul></div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Testimonial End -->

<!-- Footer Start -->
<footer id="contact">
    <div class="footer-main-container">
        <div class="container">
            <div class="footer-inner">
                <div class="footer-heading">
                    <h5>We love to hear from you.</h5>
                    <h1>Say HI </h1>

                    
                    
                </div>
                <form class="contact-from" id="form" action="https://anglara.com/submitform" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="nSBd8zk39O2CkOHslaJQu8e66lgaBCAvp1rhJJtf">                    <div class="contact-form-input contact-input-half">
                        <input type="text" name="name" required="" value="">
                        <label>NAME</label>
                        <div class="line"></div>
                        
                    </div>
                    <div class="contact-form-input contact-input-half">
                        <input type="text" name="designation" required="" value="">
                        <label>DESIGNATION</label>
                        <div class="line"></div>
                    </div>
                    <div class="contact-form-input contact-input-half">
                        <input type="text" name="company" required="" value="">
                        <label>COMPANY</label>
                        <div class="line"></div>
                    </div>
                    <div class="contact-form-input contact-input-half">
                        <input type="email" name="email" required="" value="">
                        <label>EMAIL</label>
                        <div class="line"></div>
                    </div>
                    <div class="contact-form-input contact-input-half">
                        <input type="text" name="phone" required="" value="">
                        <label>PHONE NUMBER</label>
                        <div class="line"></div>
                    </div>
                     <div class="contact-form-input contact-input-half">
                        <input type="text" name="country" required="" value="">
                        <label>COUNTRY</label>
                        <div class="line"></div>
                    </div> 

                   
                    <div class="contact-input-full contact-flex">
                        <div class="contact-input-left">
                            <div class="contact-form-input">
                                <textarea name="details" required=""></textarea>
                                <label>PROJECT SUMMARY</label>
                                <div class="line"></div>
                            </div>
                        </div>

                       
                    </div>
                    <div class="contact-input-full button">
                        <button type="submit">Send Information</button>
                    </div>
                </form>
                <div class="contact-map">
                    <iframe src="https://maps.google.com/maps?q=rvd%20education%20chandlodia%20ahmedabad%20&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="600" height="450" style="border:0" allowfullscreen=""></iframe>
                </div>
                <div class="contact-footer">
                    <div class="contact-footer-left">
                        A-27 Krushnanagar Society,<br>
Jagatpur Road, Chandlodia,<br>

                        Ahmedabad, Gujarat, India-382481<br><br>
                        rvdclasses786@gmail.com<br><br>
                        +91 9978045060
                    </div>
                    <div class="contact-footer-mid">
                        <ul class="social-list">
                            <li><a href="https://www.facebook.com/RVDEducation/" target="new">Facebook</a></li>
                            <li><a href="https://www.instagram.com/rvdeducation/" target="new">Instagram</a></li>
                            
                            
                        </ul>
                    </div>
                    <div class="contact-footer-right">
                        <img src="images/logo.png">
                        <div class="copyright">
                            © 2018 RVD Education<br>
                            All Rights Reserved.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<!-- Bootstrap Js File -->
<script src="js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery-ui.min.js"></script>

<!-- Slick Js File -->
<script type="text/javascript" src="js/slick.min.js"></script>

<!-- Appear js -->
<script src="js/appear.min.js"></script>
<script src="js/pnotify.js"></script>
<script>
    var curslid,nextslide, anm,max;
    $( ".process-control-list li" ).hover(function(e) {

        var ind = $(this).index() + 1;
        var lastind = $( ".process-control-list li.active:last" ).index() + 1;
        if(ind != lastind) {
            $( ".process-control-line-active" ).stop( true );
            var total = 0;
            var parentOffset = $(this).position();
            var wid = parentOffset.left;
            if(ind == 1) {
                total = 0;
            }
            else {
                total = $(".process-control-list li:nth-of-type("+ ind +")").position().left + ($(".process-control-list li:nth-of-type("+ ind +") .process-control-list-data").width() / 2) - 28;
            }
            $( ".process-control-line-active" ).animate({
                width: total
            }, {
                step: function( now, fx ) {
                    var cur;
                    var curwid = $(".process-control-line-active").width();
                    if(lastind < ind) {
                        $( ".process-control-list li" ).each(function() {
                            cur = $(this).position().left + ($(this).width()/2) - 32;
                            if(curwid >= cur) {
                                $(this).addClass("active");
                            }
                        });
                    }
                    else {
                        $( ".process-control-list li" ).each(function() {
                            cur = $(this).position().left + ($(this).width()/2) - 32;
                            if(curwid <= cur) {
                                $(this).removeClass("active");
                            }
                        });
                    }


                },
                duration: 600
            });
            curslid = $(".process-list li.active");
            nextslide = $(".process-list li:nth-of-type("+ind+")");
            slide1();
        }

    });
    function slide1() {
        curslid.removeClass("faded");
        setTimeout(slide2,301);
    }
    function slide2() {
        curslid.removeClass("active");
        nextslide.addClass("active");
        setTimeout(slide3,10);
    }
    function slide3() {
        nextslide.addClass("faded");
    }
    $( window ).resize(function() {
        var ind = $( ".process-control-list li.active:last" ).index() + 1;
        if(ind == 1) {
            total = 0;
        }
        else {
            total = $(".process-control-list li:nth-of-type("+ ind +")").position().left + ($(".process-control-list li:nth-of-type("+ ind +") .process-control-list-data").width() / 2) - 28;
        }
        $( ".process-control-line-active" ).css("width",total);
        max = 1;
        last = $(".process-list li.active");
        $( ".process-list li" ).each(function() {
            if($(this).addClass("active").height() > max) {
                max = $(this).addClass("active").height();
            }
            $(this).removeClass("active");
        });
        last.addClass("active");
        $(".process-list").css("min-height",max);
    });
    $( document ).ready(function() {
        max = 1;
        last = $(".process-list li.active");
        $( ".process-list li" ).each(function() {
            if($(this).addClass("active").height() > max) {
                max = $(this).addClass("active").height();
            }
            $(this).removeClass("active");
        });
        last.addClass("active");
        $(".process-list").css("min-height",max);
    });
    $('.testimonial-slider').slick({
        dots: true,
        infinite: true,
        speed: 800,
        slidesToShow: 3,
        prevArrow: false,
        nextArrow: false,
        centerMode: true,
        centerPadding: "0px",
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    centerMode: true
                }
            }
        ]
    });
    $(".contact-form-input input").focusout(function(){
        if($(this).val() != "") {
            $(this).parents(".contact-form-input").find("label").addClass("active");
        }
        else {
            $(this).parents(".contact-form-input").find("label").removeClass("active");
        }
    });
    $(".contact-form-input textarea").focusout(function(){
        if($(this).val() != "") {
            $(this).parents(".contact-form-input").find("label").addClass("active");
        }
        else {
            $(this).parents(".contact-form-input").find("label").removeClass("active");
        }
    });
    $( "#slider-range" ).slider({
        range: "min",
        value:0,
        min: 0,
        max: 10000,
        step: 1000,
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.value );
            $( ".budget-amount span" ).html( "$" + ui.value );
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "value" ) );
    $( ".budget-amount span" ).html( "$" + $( "#slider-range" ).slider( "value" ) );

        $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > '20') {
            $('.navbar-default').addClass('nav-white');
        }
        else if (scroll < '20') {
            $('.navbar-default').removeClass('nav-white');
        }
    });
    
    
    $('.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        if($(window).width() > 768)
        {
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 75
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        }
        else
        {
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 50
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        }
        if($(window).width() < 768)
        {
            $(".navbar-toggle").click();
        }
    });
    $('.animated').each(function() {
        $(this).appear(function(){

            var element = $(this);

            var animation = element.attr('data-animation');
            if(!element.hasClass('visible')){
                var animationDelay = element.attr('data-animation-delay');
                setTimeout(function(){
                            element.addClass(animation+' visible');
                        }, animationDelay
                );
            }
        });

    });
    $(".services-list.active").appear(function(){
        $(this).removeClass("active");
    });
    $('body').scrollspy({
        target: '#nav-main',
        offset: 65
    });







$( document ).ready(function() {
        var scroll = $(window).scrollTop();
        if (scroll > '20') {
            $('.navbar-default').addClass('nav-white');
        }
        else if (scroll < '20') {
            $('.navbar-default').removeClass('nav-white');
        }
    });


</script>



</body>
</html>
