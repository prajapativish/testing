<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title> RVD Education </title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top" id="nav-main">
    <div class="container">
            <img src="Images/logo1.png" height="100px" width="100px" class="nav navbar-nav navbar-left">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/RVDEducation/index.php" class="page-scroll">Home</a></li>
            <li><a href="#about" class="page-scroll">About Us</a></li>
            <li><a href="#serv" class="page-scroll">Services</a></li>
            <li><a href="#proc" class="page-scroll">Process</a></li>  
            <li><a href="#contact" class="page-scroll">Contact Us</a></li>
            <li><a href="career" class="page-scroll">Career</a></li>
        </ul>
    </div>
    </nav>
    
    <section>
    <div class="hero-main-container">
        <div class="hero-data animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="0">
            Welcome to RVD Eduaction
        </div>
    </div>
    </section>
    
    <section id="about">
    <div class="about-main-container section-container">
        <div class="container">
            <div class="about-inner">
                <div class="heading animated" data-animation="fadeInDown" data-animation-delay="0">
                    <span>
                        <h3 style="text-align:center;">About Us</h3>
                    </span>
                </div>
                <div class="about-data animated" data-animation="fadeIn" data-animation-delay="100">
                    <h3>We are from RVD Eduaction.</h3>
                    

                    <p> Welcome to RVD Education. Our goal is to spread knowledge about GTU subjects in Engineering. Here We are cover the syllabus of computer related all subjects. this education center is opened since,2016.</p>

                    
                </div>

                <h3 style="text-align:center;"> Our Purpose</h3>

                <ul class="about-list">
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="200">
                        <div class="bg-info"></div>
                        <div class="about-list-data">
                          <h4>We provide Tutorials</h4>
                            <p>We love baking wonderful web and mobile apps. </p>
                        </div>
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="300">
                        <div class="about-list-image">
                            <img src="images/about-2.png">
                        </div>
                        <div class="about-list-data">
                            <h4>Transparency</h4>
                            <p>Believe what you see.We maintain code live so clients can track development anytime.</p>
                        </div>
                    </li>
                    <li class="animated" data-animation="fadeInLeft" data-animation-delay="400">
                        <div class="about-list-image">
                            <img src="images/about-3.png">
                        </div>
                        <div class="about-list-data">
                            <h4>Best Quality</h4>
                            <p>We understand things from user's point of view, so we always comes up with the best and easiest solution.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </section>
    
    
</body>
</html>
